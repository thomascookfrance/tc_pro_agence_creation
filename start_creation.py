import os
import os.path
import sys
path = os.getcwd
sys.path.append(path)
import lib_selenium as LC
import codecs


mdp_list = []
result = open("user_to_active_result.csv", 'a', newline="\n")


def             set_mdp_list(file_name):

    with open(file_name, 'r') as data:
        line = data.readlines()
        for elem in line:
            mdp = elem
            mdp_list.append(mdp[:-1])

            
def             mailling_b2b(driver, mdp_list):

    i = 0
    for elem in mail_agence_list:
        LC.username_search(driver, elem, i, mdp_list)
        i += 1
    driver.close()

    
def     set_data(file_name):

    with open(file_name, 'r') as data:
        line = data.readlines()
        del line[0]
        for elem in line:
            data_agence = elem.split(';')
            username = data_agence[3]
            mail_agence_list.append(username)



def     init_test(file_name, mdp_file):

    index = 0
    set_mdp_list(mdp_file)
    LC.write_header(result)
    with open(file_name, 'r') as file_data:
        dd = file_data.readlines()
        del dd[0]
        for line in dd:
            data = line.split(";")
            mail_addr = data[3]
            try:
                LC.reset_pass(mail_addr)
                LC.sleep(1)
                LC.set_pass(mdp_list[index], LC.get_addr())
                LC.validate_username_adress(mail_addr, mdp_list[index])
                result.write(line[:-1] + ';' + mdp_list[index] + '\n')
                print("index: {} mail: {} pass: {}".format(index, mail_addr, mdp_list[index]))
                index += 1
            except:
                print("ERROR FOR : " + mail_addr)
    file_data.close()
    result.close()



try:
    init_test(sys.argv[1], sys.argv[2])
    result.close()
except:
    print("Usage : python3 start_tc_pro_test.py <fichier agence> <fichier mot de passe>")
