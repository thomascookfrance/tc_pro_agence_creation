import os
import sys
import email
import imaplib
from time import sleep
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from simplecrypt import encrypt, decrypt
from base64 import b64encode, b64decode

log_trace = open("log_B2B_agence.csv", "a")
log_trace_error = open("log_B2B_error.csv", "a")


def     loggin_admin(log, mdp):

        driver = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver')
        driver.get('https://b2btool-admin.peakwork.com/#/auth/login')
        log_peak = driver.find_element_by_xpath('/html/body/div/div/div/div/div/div[2]/button').click()
        sleep(1)
        user_code = driver.find_element_by_id('login_username')
        user_code.clear()
        user_code.send_keys(log)
        user_password = driver.find_element_by_id('login_password')
        user_password.clear()
        user_password.send_keys(mdp)
        user_password.submit()
        driver.implicitly_wait(10)
        reset = driver.find_element_by_xpath('/html/body/div/div/main/div/div[2]/div[1]/div[1]/form/div[6]/button[2]').click()
        return(driver)


def     reset_pass(log):

        driver = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver')
        driver.get('https://accounts.peakwork.com/de/password/reset')
        log_peak = driver.find_element_by_id("user_reset_password_username")
        sleep(1)
        log_peak.send_keys(log)
        sleep(1)
        log_peak.submit()
        sleep(0.5)
        driver.close()
        

def     set_pass(mdp, addr):

        driver = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver')
        driver.get(addr)
        sleep(1)
        mdp_box_1 = driver.find_element_by_id("user_reset_password_token_password_first")
        mdp_box_1.send_keys(mdp)
        sleep(0.5)
        mdp_box_2 = driver.find_element_by_id("user_reset_password_token_password_second")
        mdp_box_2.send_keys(mdp)
        sleep(0.5)
        valid = driver.find_element_by_id("user_reset_password_token_submit").click()
        driver.close()
        return(mdp)

        
                
def     get_addr():
        
        mail = imaplib.IMAP4_SSL('imap.gmail.com')
        (retcode, capabilities) = mail.login('thomascookdsi@gmail.com','tcfit1907')
        mail.list()
        mail.select('inbox')
        (retcode, messages) = mail.search(None, '(UNSEEN)')
        if retcode == 'OK':
                for num in messages[0].split() :
                        typ, data = mail.fetch(num,'(RFC822)')
                        for response_part in data:
                                if isinstance(response_part, tuple):
                                        original = email.message_from_bytes(response_part[1])
                                        raw_email = data[0][1]
                                        raw_email_string = raw_email.decode('utf-8')
                                        email_message = email.message_from_string(raw_email_string)
                                        if "Peakwork AG" in original['Subject']:
                                                if email_message.is_multipart():
                                                        for payload in email_message.get_payload():
                                                                if b"https://accounts.peakwork.com/fr/password/reset-token/" in payload.get_payload(decode=True):
                                                                        data = payload.get_payload(decode=True).split(b'\n')
                                                                        for elem in data:
                                                                                if b"https://accounts.peakwork.com/fr/password/reset-token/" in elem:
                                                                                        link = elem.split(b'/')
                                                                                        agence = data[1][:-1].decode("utf-8")
                                                                                        code_link = link[6][:-1].decode("utf-8")
                                                                                        complet_link = "https://accounts.peakwork.com/fr/password/reset-token/" + code_link
                                                                                        return complet_link
                                                                                        break
                                                                break



def     username_search(driver, user, index, mdp_list):

        box = driver.find_element_by_xpath('//*[@id="userTableSearchForm_id"]')
        box.send_keys(user)
        sleep(1)
        try:
                valid = driver.find_element_by_xpath('/html/body/div/div/main/div/div[2]/div[1]/div[1]/form/div[6]/button[1]').click()
                sleep(4)
                invitation = driver.find_element_by_xpath('/html/body/div[1]/div/main/div/div[2]/div[2]/table/tbody/tr/td[9]/div/button').click()
                sleep(5)
                mdp = mdp_list[index]
                adress = get_addr()
                set_pass(mdp, adress)
                validate_username_adress(user, mdp)
        except:
                log_trace_error.write('"' + user + '";"INVITATION NOT SEND"\n')
        reinit = driver.find_element_by_xpath('/html/body/div/div/main/div/div[2]/div[1]/div[1]/form/div[6]/button[2]').click()

        

def     validate_username_adress(log, mdp):

        driver = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver')
        driver.get('https://accounts.peakwork.com')
        user_code = driver.find_element_by_id('login_username')
        user_code.clear()
        user_code.send_keys(log)
        user_password = driver.find_element_by_id('login_password')
        user_password.clear()
        user_password.send_keys(mdp)
        user_password.submit()
        driver.implicitly_wait(10)
        sleep(2)
        try:
                reinit = driver.find_element_by_class_name('hpanel')
                log_trace.write('"' + log + '";"' + mdp + '"\n')
                log_out = driver.find_element_by_xpath('/html/body/div[2]/nav/div[4]/ul/li[3]/a/i').click()
                driver.close()
        except:
                log_trace.write('"' + log + '";"' + mdp +'";CREATION FAILED"\n')
                driver.close()


def     write_header(file_name):
        
        file_name.write('"AGNT_NR";"')
        file_name.write('PWOFFICE";')
        file_name.write('CREATE_PWOFFICE";')
        file_name.write('USERNAME";')
        file_name.write('NAME";')
        file_name.write('STRASSE";')
        file_name.write('LAND";')
        file_name.write('PLZ";')
        file_name.write('ORT";')
        file_name.write('EMAIL_ADRESSE";')
        file_name.write('TEL_VORWAHL";')
        file_name.write('TEL_NUMBER";')
        file_name.write('FAX_VORWAHL";')
        file_name.write('FAX_NUMBER";')
        file_name.write('VORNAME_1";')
        file_name.write('NACHNAME_1";')
        file_name.write('AGENT_EMAIL";')
        file_name.write('LANGUAGE";')
        file_name.write('POS";')
        file_name.write('CREATE_MIDOFFICE";')
        file_name.write('HOUSE_NO";')
        file_name.write('RESULT";')
        file_name.write('PWD"\n')
